import { Markets } from 'src/app/Model/markets';
import { Stores, StoreAds } from 'src/app/Model/stores';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { User } from 'src/app/Model/user';
import { ProductAds, Products } from 'src/app/Model/products';

@Injectable()
export abstract class AdminDataService {

  abstract getUsers(): Observable<User[]>;
  abstract getUserById(id): Observable<User>;
  abstract updateUser(user): Observable<User>;
  abstract createUser(user): Observable<User>;
  abstract deleteUser(id);

  abstract getStores(): Observable<any>;
  abstract getStoresById(id);
  abstract createStores(store): Observable<Stores>;
  abstract updateStore(store, id): Observable<any>;
  abstract deleteStore(id);

  abstract getMarkets(): Observable<any>;
  abstract getMarketById(id): Observable<any>;
  abstract updateMarket(market): Observable<Markets>;
  abstract deleteMarket(id);
  abstract createMarket(market): Observable<Markets>;

  abstract getOrderDetails();
  abstract getPaymentDetails();

  abstract getProducts();
  abstract getProductById(id): Observable<Products>;
  abstract addProduct(product): Observable<Products>;
  abstract updateProduct(product): Observable<Products>;
  abstract deleteProduct(id);
  abstract uploadFile(file);
  abstract getImage(): Observable<any>;
  abstract getImageById(id): Observable<any>;
  abstract getOrderByStoreId(id);
  abstract getOrders(): Observable<any>;

  abstract uploadStoreImage(file);
  abstract getStoreImage();
  abstract getStoreImageById(id);
  abstract updateStoreImage(file, id);

  abstract fundWallet(wallet);
  abstract postProductAds(products): Observable<ProductAds>;
  abstract getProductAds();
  abstract getProductAdsById(id);
  abstract getWallet(id);
  abstract updateWallet(balance, id);

  abstract postStoreAds(store): Observable<StoreAds>;
  abstract getStoreAds();
  abstract getStoreAdsById(id);

  abstract postPromotionPackage(promotion);
  abstract getPromotionPackages();
  abstract getPromotionPackagesById(id);
  abstract updatePromotionPackages(promotion, id);
  abstract deletePromotionPackages(id);
  abstract deleteStoreAds(id);
  abstract deleteProductAds(id);
  abstract deleteDiscountAds(id);

  abstract uploadMarketImage(file);
  abstract uploadHero(file);
  abstract getHero();
  abstract updateHero(file, id);
}
