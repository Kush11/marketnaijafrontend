import { Products } from './../../../Model/products';

import { MerchantService } from './../merchant.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { AdminDataService } from '../../admin-services/admin.data.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { AuthenticateDataService } from '../../../services/Authentication/authentication.data.service';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';


@Component({
  selector: 'app-merchant',
  templateUrl: './merchant.component.html',
  styleUrls: ['./merchant.component.css']
})
export class MerchantComponent implements OnInit {

  constructor(@Inject(LOCAL_STORAGE) private localStorage: any,  private router: Router) { }

  ngOnInit() {
  }

  goToStore() {
    const storeId = this.localStorage.getItem('storeId');
    this.router.navigate(['store', storeId]);
  }

  onClickTerms() {
    this.router.navigate(['terms']);
  }
  onClickAbout() {
    this.router.navigate(['about']);
  }
  onClickContacts() {
    this.router.navigate(['contact']);
  }
  onClickMarket() {
    this.router.navigate(['markets']);
  }
  onClickPolicy() {
    this.router.navigate(['policy']);
  }
}
