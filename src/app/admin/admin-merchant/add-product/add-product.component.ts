import { StoreService } from 'src/app/services/Shared/store.service';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { AdminWebService } from './../../admin-services/admin.web.service';
import { Component, OnInit, ViewChild, ElementRef, OnDestroy, Output, Inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { EventEmitter } from 'events';
import { AdminDataService } from '../../admin-services/admin.data.service';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit, OnDestroy {
  productForm: FormGroup;
  createDate: string;
  loading = false;
  private unsubscribe$ = new Subject<void>();
  @ViewChild('fileInput') fileInput:any;
  storeId: any;
  uploadStatus: number;
  progress: number;
  message: string;
  fileToUpload: File = null;
  uploadResponse = { status: '', message: '', filePath: '' };
  error: any;
  formData: void;
  productUrl: any;
  productId: number;
  submitted: boolean;

  constructor(@Inject(LOCAL_STORAGE) private localStorage: any,  public activeModal: NgbActiveModal,
               private formBuilder: FormBuilder,
               public storeWebService: AdminDataService,
               private storeService: StoreService
               ) { }

  ngOnInit() {
    this.createDate = new Date().toDateString();
   this.createForm();
  }


  createForm() {
    this.storeId = this.localStorage.getItem('storeId');
    console.log('store id', this.storeId);
    this.productForm = this.formBuilder.group({
      productName: ['', Validators.required],
      Description: ['', Validators.required],
      Price: ['', Validators.required],
      Discount: ['', Validators.required],
      Brand: ['', Validators.required],
      createdOn: [this.createDate, Validators.required],
      Category: ['', Validators.required],
      Specification: ['', Validators.required],
      location: ['', Validators.required],
      storeId: [this.storeId],
      // file: [null]
    });
  }
  async addProduct() {
    const formModel = this.productForm.value;
    this.loading = true;
    await this.storeWebService.addProduct(formModel)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(data => {
      setTimeout(() => {
         this.storeWebService.getProducts().subscribe(products => {
          const productId = products[products.length - 1];
          this.productId = productId.productId;
          const image = {file: this.productUrl, productId: this.productId};

          this.storeWebService.uploadFile(image).subscribe(res => {

            alert('Product added!');
            console.log('image', res);
            this.loading = false;
            this.submitted  = true;
            this.storeService.getsubmittedStatus(this.submitted);
          });
        });
        alert('Product Id generated, Uploading image!');
        this.loading = true;

      }, 1000);
    });
    // const image = this.imageData;
  }

  uploadPhoto(event) {
    const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = async () => {
        const file = reader.result.toString().split(',')[1];
        const model = {File : file};
        this.productUrl = model.File;
      };
  }
  clearFile() {
    this.fileInput.nativeElement.value = '';
  }
  closeModal() {
    this.activeModal.close('Modal Closed');

  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
