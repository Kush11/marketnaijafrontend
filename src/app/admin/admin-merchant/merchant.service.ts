import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class MerchantService {

  _storeSource = new Subject();
  _productSource = new Subject();

  public store = this._storeSource.asObservable();
  public product = this._productSource.asObservable();

  constructor() {}

  async publishStore(store) {
    await this._storeSource.next(store);

  }
  async publishProducts(product) {
    await this._productSource.next(product);
  }
}
