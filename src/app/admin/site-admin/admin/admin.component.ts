import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdminDataService } from '../../admin-services/admin.data.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { MarketsDataService } from 'src/app/services/Markets/markets.data.service';
import { AuthenticateWebService } from 'src/app/services/Authentication/authentication.web.service';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit, OnDestroy {
  public packageForm: FormGroup;
  private unsubscribe$ = new Subject<void>();
  public promotionPackages: any;
  showAds: boolean;
  storeAds: any;
  productAds: any;
  DiscountAds: any;
  marketsCount: number;
  allMarkets: any;
  storesCount: number;
  stores: any;
  productPromotions: any;
  allStores = [];
  totalActiveAds: any;
  promoCount: any;
  productUrl: string;
  public marketForm: FormGroup;
  marketId: any;
  loading: boolean;
  showMarketForm: any;
  banner1: string;
  banner3: string;
  banner2: string;
  siteHero: any;


  constructor(private formBuilder: FormBuilder,
    private router: Router, private authService: AuthenticateWebService,
    private adminService: AdminDataService,
    private marketService: MarketsDataService ) { }

  ngOnInit() {

    this.getMarkets();
    this.getStores();
    this.packageForm = this.formBuilder.group({
      packageName: ['', Validators.required],
      duration: ['', Validators.required],
      price: ['', Validators.required]
    });
    this.marketForm = this.formBuilder.group({
      marketName: ['', Validators.required],
      description: ['', Validators.required]
    });
    this.getPromotionStores();
    this.getproductPromo();
    this.getPromotionPackages();
  }


  adminLogout() {
    this.authService.logout();
    this.router.navigate(['']);
  }
  toggleAds() {
    this.showAds = !this.showAds;
  }

  toggleMarketForm() {
    this.showMarketForm = !this.showMarketForm;
  }
  onClickStore(id) {
    this.router.navigate(['store', id]);
  }
  getMarkets() {
    this.adminService.getMarkets()
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(markets => {
      this.marketsCount = markets.length;
      this.allMarkets =  markets;
      this.allMarkets.forEach(element => {
        this.allStores.push(element.stores);

      });
    });
  }
  createMarket() {
    this.adminService.createMarket(this.marketForm.value)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(market => {
      setTimeout(() => {
        this.adminService.getMarkets().subscribe(markets => {
         const marketId = markets[markets.length - 1];
         this.marketId = marketId.marketId;
         const image = {marketBanner: this.productUrl, marketId: this.marketId};

         this.adminService.uploadMarketImage(image).subscribe(res => {

           alert('Market added!');
           this.loading = false;
         });
       });
       alert('Market Id generated, Uploading image!');
       this.loading = true;
       this.getMarkets();
     }, 1000);
    },
    error => {
      alert('An error occured while creating market');
      this.loading = false;
    });

  }


  deleteMarket(id) {
    this.adminService.deleteMarket(id)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(res => {
      alert('market deleted successfully');
      this.getMarkets();
    },
    error => {
      alert('An error occured. Please tryagain later');
    });
  }

  async getStores() {
   await this.marketService.getAllStores()
   .pipe(takeUntil(this.unsubscribe$))
   .subscribe(data => {
     this.stores = data.filter(cs => cs.confirmStatus === true);
     this.storesCount = this.stores.length;
   });
  }

  getActiveAds(storeAd?, productAd?) {
    this.totalActiveAds = storeAd.length + productAd.length;
  }

  createPromotionPackage() {
    const  promotion = this.packageForm.value;
    this.adminService.postPromotionPackage(promotion)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(res => {
      alert('Package added successfully');
      this.getPromotionPackages();
    },
    error => {
      alert('An error occurred while generating package. Please try again shortly');
    });
  }

  async getPromotionPackages() {
    await this.adminService.getPromotionPackages()
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(data => {
      this.promotionPackages = data;
    });
  }
  getPromotionStores() {
    this.adminService.getStoreAds()
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(storeAds => {
      this.storeAds = storeAds;
       this.promoCount = this.storeAds.length;
      this.getproductPromo();
    });
  }
  getproductPromo() {
    this.adminService.getProductAds()
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(product => {
      const productAds = product;
      const discounts = product;
      this.productAds = productAds.filter(s => s.discount === false);
      this.DiscountAds = discounts.filter(dis => dis.discount === true);
    });
  }

  onStoreClick(id) {
    this.router.navigate(['store', id]);
  }
  deleteStoreAd(id) {
    this.adminService.deleteStoreAds(id)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(del => {
      this.getPromotionStores();
      alert('Promotion Deactivated');
    });
  }
  deleteProductAd(id) {
    this.adminService.deleteProductAds(id)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe( res => {
      this.getproductPromo();
      alert('Promotion Deactivated');
    });
  }
  deleteDiscountAd(id) {
    this.adminService.deleteDiscountAds(id)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe( res => {
      this.getproductPromo();
      alert('Promotion Deactivated');
    });
  }

  uploadPhoto(event) {
    const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = async () => {
        const file = reader.result.toString().split(',')[1];
        const model = {File : file};
        this.productUrl = model.File;
      };
  }

  uploadBanner1(event) {
    const reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = async () => {
      this.loading = true;
      const file = reader.result.toString().split(',')[1];
      const model = {File : file};
      if (model.File) {
        this.loading = false;
        this.banner1 = model.File;
        alert('Banner Saved');
      }
    };

  }
  uploadBanner2(event) {
    const reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = async () => {
      this.loading = true;
      const file = reader.result.toString().split(',')[1];
      const model = {File : file};
      if (model.File) {
        this.loading = false;
        this.banner2 = model.File;
        alert('Banner Saved');
      }
    };
  }
  uploadBanner3(event) {
    const reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = async () => {
      this.loading = true;
      const file = reader.result.toString().split(',')[1];
      const model = {File : file};
      if (model.File) {
        this.loading = false;
        this.banner3 = model.File;
        alert('Banner Saved');
      }

    };
  }
  getHero() {
    this.adminService.getHero()
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(data => {
      const siteHero = data;
      this.siteHero = siteHero;
      return this.siteHero;
    });

  }
  createSiteHero() {
    this.getHero();
    const image = {banner_one: this.banner1, banner_two: this.banner2, banner_three: this.banner3};
    if (!this.getHero) {
      this.loading = true;

    this.adminService.uploadHero(image)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe( res => {
      this.getHero();
      alert('Site banners uploaded successfully');
      this.loading = false;
    },

    error => {
      alert('An error occurred while uploading banners');
      this.loading = false;
    });

    } else {
      this.adminService.getHero()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(hero => {
        const id = hero.fileuploadid;
      this.loading = true;
      this.adminService.updateHero(image, id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(data => {
        alert('Site Banner Updated');
        this.loading = false;
      });
    });
    }

  }
  deleteStore(id) {
    const updateStore = {ConfirmStatus: false};
    this.adminService.updateStore(updateStore, id)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(data => {
      this.getStores();
      alert('Store ' + id + ' has been removed sucessfully');
    }, error => {
      alert('An errror occured, please try again later');
    });
  }
  onProductClick(id) {
    this.router.navigate(['productDetails', id]);
  }

  onClickTerms() {
    this.router.navigate(['terms']);
  }
  onClickAbout() {
    this.router.navigate(['about']);
  }
  onClickContacts() {
    this.router.navigate(['contact']);
  }
  onClickMarket() {
    this.router.navigate(['markets']);
  }
  onClickPolicy() {
    this.router.navigate(['policy']);
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
