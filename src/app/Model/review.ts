export class Review {
  Id: any;
  Comments: string;
  Rating: number;
  UserId: number;
  StoreId: number;
  CommentedOn: string;
  UserName: string;
}
