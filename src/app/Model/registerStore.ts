export class RegisterStore {
  storeName: string;
  firstName: string;
  lastName: string;
  email: string;
  createdOn: string;
  marketId: number;
  storeUrl: string;
  status: boolean;
  phoneNumber: string;
  address: string;
  storeType: string;
  rating: number;
}
