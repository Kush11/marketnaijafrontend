import { Stores } from './stores';

export class User {

  id: any;
  username: string;
  password: string;
  phoneNumber: string;
  address: string;
  token?: string;
  role: string;
  stores: Stores;

}
