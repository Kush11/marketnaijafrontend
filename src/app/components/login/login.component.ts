import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first, takeUntil } from 'rxjs/operators';
import { Role } from 'src/app/Model/role';
import { AuthenticateDataService } from 'src/app/services/Authentication/authentication.data.service';
import { Subject } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { WINDOW } from '@ng-toolkit/universal';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  data: Object;
  spinner = false;
  private unsubscribe$ = new Subject<void>();

  constructor(@Inject(WINDOW) private window: Window, private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              public activeModal: NgbActiveModal,
              private authenticate: AuthenticateDataService

            ) {
                // redirect to home if already logged in
                if (this.authenticate.currentUserValue) {
                  this.router.navigate(['/']);
                }
               }

  ngOnInit() {


    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      email: ['', Validators.required],
      role: [Role.User, Validators.required],
      address: ['', Validators.required]

    });



    // get return url from route or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convinience getter for easy access to form fields
  get f() {return this.loginForm.controls; }
  // get x() {return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    // // stop here if form is invalid
    // if (this.loginForm.invalid) {
    //   return;
    // }
    this.loading = true;
    this.authenticate.login(this.f.username.value, this.f.password.value)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(
      data => {
        this.loadSpinner();
      },
      error => {
        this.error = error;
        this.loading = false;
      }
    );
  }
  onRegister() {
    this.submitted = true;
    this.loading = true;
    this.authenticate.register(this.loginForm.value)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(
      data => {
        this.loading = false;
        alert('Account created sucessfully.');
        this.authenticate.login(this.f.username.value, this.f.password.value)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          data => {
            this.loadSpinner();
          },
          error => {
            this.error = error;
            this.loading = false;
          }
        );
      },
      error => {
        this.loading = false;
        alert('An error occured. Please try again shortly.');
      }
    );
  }
  closeModal() {
    this.activeModal.close('Modal Closed');
  }
  loadSpinner() {
    this.activeModal.close('Modal Closed');
      this.window.location.reload();
  }
  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
