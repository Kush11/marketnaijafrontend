import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MarketsDataService } from 'src/app/services/Markets/markets.data.service';

@Component({
  selector: 'app-markets',
  templateUrl: './markets.component.html',
  styleUrls: ['./markets.component.css']
})
export class MarketsComponent implements OnInit {
  public markets;
  emptyMarket: boolean;
  isCollapsed: boolean;
  marketImage = [];
  searchText: any;
  loadSpinner: boolean;

  constructor(private marketService: MarketsDataService, private router: Router) {

  }

  ngOnInit() {
    this.loadSpinner = true;
    this.isCollapsed = true;
    this.marketService.getAllMarkets().subscribe(market => {
     this.markets = market;
     this.loadSpinner = false;
     if (this.markets.length === 0) {
      this.emptyMarket = true;
      this.loadSpinner = false;
     }
    });
  }

  getMarket(marketId) {
    this.router.navigate(['markets', marketId]);
  }

}
