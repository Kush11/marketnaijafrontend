import { AdminDataService } from 'src/app/admin/admin-services/admin.data.service';
import { AuthenticateDataService } from 'src/app/services/Authentication/authentication.data.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { MarketsDataService } from 'src/app/services/Markets/markets.data.service';
import { first, takeUntil } from 'rxjs/operators';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { Role } from 'src/app/Model/role';
import { Subject } from 'rxjs';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';


@Component({
  selector: 'app-register-store',
  templateUrl: './register-store.component.html',
  styleUrls: ['./register-store.component.css']
})
export class RegisterStoreComponent implements OnInit, OnDestroy {
  storeUrl: any;
  merchantForm: FormGroup;
  marketData: any;
  createDate: any;
  submitted: boolean;
  merchantRoleForm: FormGroup;
  userId: any;
  user: any;
  username: any;
  address: any;
  email: any;
  error: any;
  private unsubscribe$ = new Subject<void>();
  defaultImage: string;
  productUrl: string;
  storeId: any;
  showBusinessLocation = false;
  pic: string;
  allowReg: boolean;

  constructor(@Inject(LOCAL_STORAGE) private localStorage: any,
    private formBuilder: FormBuilder,
    private storeWebService: MarketsDataService,
    public activeModal: NgbActiveModal,
    private router: Router,
    private authService: AuthenticateDataService,
    private adminService: AdminDataService
  ) {}

  ngOnInit() {
    this.getUserDetails();
    this.createDate = new Date().toDateString();
    this.getStoreDetails();
    this.merchantForm = this.formBuilder.group({
      storeName: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      createdOn: [this.createDate, Validators.required],
      marketId: [''],
      storeUrl: ['', Validators.required],
      confirmStatus: [true, Validators.required],
      phoneNumber: ['', Validators.required],
      address: ['', Validators.required],
      storeType: ['', Validators.required],
      rating: [1, Validators.required],
      userId: [this.userId, Validators.required],
    });
    this.merchantRoleForm = this.formBuilder.group({
      id: [this.userId, Validators.required],
      // username: [this.username, Validators.required],
      // email: [this.email, Validators.required],
      role: [Role.Merchant, Validators.required],
      // address: [this.address, Validators.required],
    });

  }

  get f() {return this.merchantForm.controls; }

  async getStoreDetails() {
    await this.storeWebService.getAllMarkets().subscribe(markets => {
      this.marketData = markets;
    });
  }

  async registerStore() {
        await this.authService.update(this.merchantRoleForm.value)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          userData => {
            if (!this.merchantForm.value) {
              alert('Please fill all fields');
              return;
            } else {
              this.storeWebService
              .createStore(this.merchantForm.value)
              .pipe(takeUntil(this.unsubscribe$))
              .subscribe(
                store => {
                  alert('Your store has been created successfully');
                  this.closeModal();
                  this.localStorage.removeItem('currentUser');
                  location.reload();
                  alert('Please login to access you dashboard');
                  this.router.navigate(['login']);
                },
                error => {
                  alert(
                    'An error occured while creating your store. Please try again shortly.'
                  );
                }
              );
            }
          },
          error => {
            alert('An error occurred while creating your store, please try again shortly.');
          }
        );
  }
  getStoreType(value) {
    if (value) {
      this.allowReg = true;
    }
    if (value === 'physical store') {
      this.showBusinessLocation =  true;
    } else {
      this.showBusinessLocation  = false;
      this.storeWebService.getAllMarkets().subscribe(markets => {
        this.marketData = markets[0];
        this.merchantForm.patchValue({
          marketId: this.marketData.marketId
        });
      });
      return;
    }
  }

  closeModal() {
    this.activeModal.close('Modal Closed');
  }
  getUserDetails() {
    this.user = JSON.parse(this.localStorage.getItem('currentUser'));
    if (this.user) {
      this.username = this.user.username;
      this.address = this.user.address;
      this.email = this.user.email;
      this.userId = this.user.id;
    } else {
      alert('Please Login or Create an account if you do not have one');
      this.closeModal();
      this.router.navigate(['login']);
    }
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
