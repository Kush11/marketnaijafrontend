import { LoginComponent } from './../login/login.component';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MarketsDataService } from 'src/app/services/Markets/markets.data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { AdminDataService } from 'src/app/admin/admin-services/admin.data.service';
import { StoreService } from 'src/app/services/Shared/store.service';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();
  routeId: number;
  product: any;
  adsNewPrice = [];
  newPrice: number;
  orderDate: string;
  orderForm: FormGroup;
  userId: any;
  productId: any;
  storeId: any;
  loading: boolean;
  user: any;
  renderForm: boolean;
  productImage: any;
  featuredProducts: any;
  promoProducts: any;
  image = [];
  newPromoPrice = [];
  showSlasher = [];

  constructor(@Inject(LOCAL_STORAGE) private localStorage: any,
    private route: ActivatedRoute,
    private router: Router,
    private storeService: MarketsDataService,
    public formBuilder: FormBuilder,
    private modalService: NgbModal,
    private adminService: AdminDataService,
    private routerService: StoreService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(p => {
      this.routeId = p['id'];
      console.log('product Id', this.routeId);
    });
    this.getUserId();
    this.getProductById();
    this.getStoreId();
    this.getFeaturedProducts();
    this.createOrderDate();
  }

  get f() {
    return this.orderForm.controls;
  }

  getProductById() {
    const id = this.routeId;
    this.storeService
      .getProductById(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(data => {
        console.log(data);
        this.product = data;
        this.storeId = this.product.storeId;
        this.productId = this.product.productId;
        const discount = this.product.discount;
        const realPrice = this.product.price;
        this.newPrice = realPrice - (discount / 100) * realPrice;
      });
      this.adminService.getImageById(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(image => {
        this.productImage = 'data:image/png;base64,' + image.file;
      });
  }
  getUserId() {
    const user = JSON.parse(this.localStorage.getItem('currentUser'));
    if (user) {
      this.user = user;
      this.userId = user.id;
      console.log('customer id', this.userId);
      this.renderForm = true;
    } else {

      this.renderForm = false;
      return;
    }
  }
  goToStore(){
    const id = this.routeId;
    this.storeService
    .getProductById(id)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(data => {
      this.product = data;
      this.storeId = this.product.storeId;
      this.router.navigate(['store', this.storeId])
    });

  }
  getStoreId() {
    const id = this.routeId;
    this.storeService
    .getProductById(id)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(data => {
      this.product = data;
      this.storeId = this.product.storeId;
      return this.storeId;
    });
  }

  onSubmitOrder() {
  // console.log('i can get the storeId', this.storeId);

    this.user = this.localStorage.getItem('currentUser');
    if (this.user !== null) {
      const orderDetails = {customerId: this.userId, productId: this.routeId,
        storeId: this.storeId, orderDate: this.orderDate, orderStatus: 'Pending'};
      this.loading = true;
      this.storeService
        .submitOrder(orderDetails)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          data => {
            alert(
              'Thank You, your order has been sent. The merchant will get in touch with you shortly.'
            );
            this.loading = false;
            this.router.navigate(['store', this.storeId]);
          },
          error => {
            alert(
              'Sorry an error occured while we were trying to place your order'
            );
            this.loading = false;
          }
        );
    } else {
      this.loading = false;
      alert('Please login or create an account if you do not have one.');
      this.openFormModal();
    }
  }

  createOrderDate() {
    this.orderDate = new Date().toDateString();
  }
  openFormModal() {
    const modalRef = this.modalService.open(LoginComponent);
    modalRef.result
      .then(result => {
      })
      .catch(error => {
        console.log(error);
      });
  }

  onPlaceOrder() {
    this.openFormModal();
  }

  goBack() {
    this.routerService.getPreviousUrl();
  }
  getFeaturedProducts() {
    this.adminService.getProductAds()
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(res => {
      const products = res;
      this.promoProducts = products.filter(c => c.StoreId = this.storeId);
      this.promoProducts.forEach(element => {
        const productImage = element.products.file.file;
        const image = 'data:image/png;base64,' + productImage;
        this.image.push(image);
        const price = element.products.price;
        const discount = element.products.discount;
        if (discount !== 0) {
          const newPromoPrice = price - (discount / 100) * price;
          this.newPromoPrice.push(newPromoPrice);
          const showSlasher = true;
          this.showSlasher.push(showSlasher);
        } else {
            const newPromoPrice = price;
            this.newPromoPrice.push(newPromoPrice);
            const showSlasher = false;
            this.showSlasher.push(showSlasher);
        }

      });
    });
    }
    gotoProduct(id) {

      this.router.navigate(['productDetails', id]);
      this.router.routeReuseStrategy.shouldReuseRoute = function () {
        return false;
      };
    }
    ngOnDestroy() {
      this.unsubscribe$.next();
      this.unsubscribe$.complete();
    }
}
