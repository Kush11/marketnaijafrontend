import { LoginComponent } from './../login/login.component';
import { ProductDetailsComponent } from './../product-details/product-details.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StoreService } from './../../services/Shared/store.service';
import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { CartService } from 'src/app/services/Shared/cart.service';
import { Router, ActivatedRoute } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MarketsDataService } from 'src/app/services/Markets/markets.data.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer } from '@angular/platform-browser';
import { AdminDataService } from 'src/app/admin/admin-services/admin.data.service';
import { saveAs } from 'file-saver';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';


interface IRating {
  id: number;
  rating: number;
  username: string;
  commentedOn: string;
  comment: string;
}

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit, OnDestroy {
  storeDetails: any;
  routeId: number;
  storeName: any;
  commentForm: FormGroup;
  customerForm: FormGroup;
  storeId: any;
  userId: any;
  store: any;
  submitted: boolean;
  loading: boolean;
  orderForm: FormGroup;
  marketId: any;
  storeReview: any;
  commentDate: string;
  userName: any;
  storeType: any;
  storeAddress: any;
  storeEmail: any;
  storePhone: any;
  private unsubscribe$ = new Subject<void>();
  userAddress: any;
  userPhone: any;
  isEmpty: boolean;
  emptyStore: boolean;
  storeDescription: any;
  checkBoxValue: any = false;
  ratingClicked = 0;
  itemIdRatingClicked: string;
  itemReview = [];
  helperArray: Array<any> = [];
  showItems: IRating[];
  imageToShow: any;
  reviewLoading: boolean;


  items: IRating[] = [
    { 'id': 0, 'rating': 0, 'comment': '', 'commentedOn': '', 'username': ''}
  ];
  userRating: any;
  review: [];
  ratings: number[];
  newReview: any;
  reviewId: any;
  isDisabledState = true;
  products: any;
  discounts: any = [];
  imagePath: any = [];
  isImageLoading: boolean;
  mySrc: any;
  attachmentFileName: any;
  productImage: any;
  storeImage: any;
  image: any;
  productId: any;
  showSlasher = [];
  searchText: any;
  storeOwner: any;
  isRegistered: boolean;
  isDiscount = [];

  constructor(@Inject(LOCAL_STORAGE) private localStorage: any,
    private cartService: CartService,
    private route: ActivatedRoute,
    private storeService: MarketsDataService,
    private routeService: StoreService,
    private formBuilder: FormBuilder,
    private router: Router,
    private modalService: NgbModal,
    private adminStore: AdminDataService
  ) {
    this.getStoreSearch();
  }

  ngOnInit() {
    this.localStorage.removeItem('User Rating');
    this.createReviewDate();
    this.route.params.subscribe(p => {
      this.routeId = p['id'];
    });

    this.getCurrentUser();
    this.getStores();
    this.getStoreImage();
    this.customerForm = this.formBuilder.group({
      userName: [this.userName, Validators.required],
      createdOn: [this.commentDate, Validators.required],
      storeId: [this.routeId],
      userId: [this.userId, Validators.required]
    });
    this.commentForm = this.formBuilder.group({
      comment: ['', Validators.required],
      storeId: [this.routeId],
      commentedOn: [this.commentDate],
      username: [this.userName],
      userRate: [this.ratingClicked]

    });

    // this.getImageFromService();

  }

  get f() {return this.commentForm.controls; }


  addcartItem(id) {
    this.cartService.addItems();
  }

  getCurrentUser() {
    const user = JSON.parse(this.localStorage.getItem('currentUser'));
    if (!user) {
      this.userName = 'Anonymous';
    } else {
      this.userName = user.username;
      this.userId = user.id;
      this.userAddress = user.address;
    }
  }

  getStores() {
    this.storeService.getStoresById(this.routeId)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(store => {
      if (store) {
        this.emptyStore = false;
        this.isEmpty = false;
        this.storeDetails = store;
        this.helperArray.splice(0);
        this.storeName = this.storeDetails['storeName'];
        this.marketId = this.storeDetails['marketId'];
        this.storeReview =  this.storeDetails['review'];
        this.storePhone = this.storeDetails['phoneNumber'];
        this.storeEmail = this.storeDetails['email'];
        this.storeAddress = this.storeDetails['address'];
        this.storeType = this.storeDetails['storeType'];
        this.storeDescription = this.storeDetails['Description'];
        this.storeOwner = this.storeDetails['user'];

        // const storeImage = this.storeDetails['storeImage'];
        // this.storeImage = storeImage.file;
        this.routeService.createUrl(this.storeName, this.marketId);
        this.storeReview.map(element => {
          const review = element.userRate;
          const reviewId = element.id;
          const username = element.userName;
          const comment = element.comment;
          const commentOn = element.commentedOn;

          this.showItems = [
            {'id': reviewId, 'rating': review, 'username': username, 'commentedOn': commentOn, 'comment': comment }
          ];
          this.helperArray.push(this.showItems);
        },
        error => {
          this.emptyStore = true;
          this.isEmpty = true;
        });
        this.products = this.storeDetails.products;
        this.products.forEach(element => {
          const discount = element.discount;
          if (discount !== 0) {
            const isDiscount = true;
            this.productId = element.productId;
            const originalPrice = Number(element.price);
            const newPrice = (originalPrice - (discount / 100) * originalPrice);
            this.discounts.push(newPrice);
            this.isDiscount.push(isDiscount);
          } else {
              const isDiscount = false;
              const newPrice = Number(element.price);
              this.discounts.push(newPrice);
              this.isDiscount.push(isDiscount);
          }
        });
      }
    });
  }

  openProductDetail(id) {
   this.router.navigate(['productDetails', id]);
  }
  ratingComponentClick(clickObj: any) {
    const item = this.items.find(((i: any) => i.id === clickObj.itemId));
    if (!!item) {
      item.rating = clickObj.rating;
      this.ratingClicked = item.rating;
    }
  }

  gotoPrevious(id) {
   this.router.navigate(['markets', id]);
  }

   async onSubmit() {
    this.reviewLoading = true;
    this.commentForm.reset({
      comment: this.commentForm.value.comment,
      storeId: this.routeId,
      commentedOn: this.commentDate,
      username: this.userName,
      userRate: this.ratingClicked
    });
    await this.storeService.AddReview(this.commentForm.value)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(data => {
       this.getStores();
       setTimeout(() => {
        this.reviewLoading = false;
       }, 20000);
       alert('Review submitted');
    },
    error => {
      alert('oops!!!, sorry an error occurred while we were posting your review. Please try again shortly.');
      this.reviewLoading = false;
    });
  }
  paymentCancel() {
    console.log('Payment was Canceled');
  }
  paymentDone(event) {
    console.log('Payment Done');
  }
  createReviewDate() {
    this.commentDate = new Date().toDateString();
  }
  getStoreCustomers() {
    const user = JSON.parse(this.localStorage.getItem('currentUser'));
    this.storeService.getCustomerById(this.routeId)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(res => {
     res.forEach(element => {
       const customer = element.userId;
       if (customer === user.id) {
          this.isRegistered = true;
       }
      });
      this.createCustomer(this.isRegistered);
    });
  }
  createCustomer(isRegistered) {
    this.isRegistered = isRegistered;
    const user = JSON.parse(this.localStorage.getItem('currentUser'));
    if (user !== null) {
      if (this.isRegistered !== true) {
        this.storeService.createCustomer(this.customerForm.value)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(data => {
            alert('Registration Successful');
        },
        error => {
          alert('Something went wrong, Please try again shortly');
        });
      } else {
        alert('You are already a customer of this store');
      }
    } else {
      // alert('You have to log in to become a customer');
      const modalRef = this.modalService.open(LoginComponent);
      modalRef.result.then((result) => {
        console.log(result);
      }).catch((error) => {
        console.log(error);
      });
      // this.router.navigate(['login']);
    }
  }
  async getStoreImage() {
    await this.adminStore.getStoreImageById(this.routeId).subscribe(img => {
      if(img){
        this.storeImage = 'data:image/png;base64,' + img.file;
      }
      else{
        return;
      }
    });
  }

   getStoreSearch() {
     this.routeService.filterSearch
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(data => {
      this.searchText = data;
    });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
