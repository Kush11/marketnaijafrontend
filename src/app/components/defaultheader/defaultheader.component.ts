import { MarketsDataService } from 'src/app/services/Markets/markets.data.service';

import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticateWebService } from '../../services/Authentication/authentication.web.service';
import { first } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RegisterStoreComponent } from '../register-store/register-store.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StoreService } from 'src/app/services/Shared/store.service';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';


@Component({
  selector: 'app-defaultheader',
  templateUrl: './defaultheader.component.html',
  styleUrls: ['./defaultheader.component.css']
})
export class DefaultheaderComponent implements OnInit {

  isLoggedIn$: Observable<boolean>;
  loggedIn: any;
  userId: any;
  public isCollapsed: boolean;
  storeId: number;
  filter: string;

  constructor(@Inject(LOCAL_STORAGE) private localStorage: any,  private router: Router, private authService: AuthenticateWebService,
    private storeService: MarketsDataService, private sharedService: StoreService,
    private formBuilder: FormBuilder, private modalService: NgbModal) { }

  ngOnInit() {
    this.loggedIn = JSON.parse(this.localStorage.getItem('currentUser'));

    // this.isLoggedIn$ = this.authService.isLoggedIn;
    this.isCollapsed = true;
    this.router.events.subscribe( (event) => {
      if (event instanceof NavigationStart) {
         this.isCollapsed = false;
      }

      if (event instanceof NavigationEnd) {
          this.isCollapsed = true;
      }

      if (event instanceof NavigationError) {
          // Hide loading indicator
          // Present error to user
          this.isCollapsed = true;
      }
  });
  }

  searchStore() {
   this.sharedService.getFilterParameters(this.filter);
  }
  onLogout() {
    this.authService.logout();
    location.reload();
  }

  onClick() {
    this.router.navigate(['login']);
  }
  openMerchantDashboard() {
    this.userId = this.loggedIn.id;
    if (this.loggedIn) {
      this.storeService.getStoresByUserId(this.userId).pipe(first()).subscribe(data => {
        const storeId = data;
        storeId.forEach(element => {
          const storeData = element.storeId;
          this.storeId = storeData;
           this.router.navigate(['admin/store/', this.storeId]);
        });
      });
    } else {
      this.router.navigate(['login']);
    }
  }
  openFormModal() {
    const modalRef = this.modalService.open(RegisterStoreComponent);

    modalRef.result.then((result) => {
    }).catch((error) => {
      console.log(error);
    });
  }
}
