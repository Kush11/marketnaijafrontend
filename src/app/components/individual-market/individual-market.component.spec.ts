import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndividualMarketComponent } from './individual-market.component';

describe('IndividualMarketComponent', () => {
  let component: IndividualMarketComponent;
  let fixture: ComponentFixture<IndividualMarketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndividualMarketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndividualMarketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
