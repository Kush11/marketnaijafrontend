import { AdminDataService } from 'src/app/admin/admin-services/admin.data.service';
import { MarketsDataService } from 'src/app/services/Markets/markets.data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, Inject } from '@angular/core';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';

@Component({
  selector: 'app-individual-market',
  templateUrl: './individual-market.component.html',
  styleUrls: ['./individual-market.component.css']
})
export class IndividualMarketComponent implements OnInit {


  public routeId: any;
  public market: any;
  public stores: any;
  public marketName: any;
  description: any;
  store: any;
  isCollapsed: boolean;
  emptyMarket: boolean;
  storeImage: any;
  storeId: any;
  defaultImage: string;
  pic: string;
  searchText: any;
  loadSpinner: boolean;
  constructor(@Inject(LOCAL_STORAGE) private localStorage: any, private route: ActivatedRoute,
     private marketDetails: MarketsDataService,
     private adminService: AdminDataService,
     private router: Router) { }

  ngOnInit() {
    this.isCollapsed = true;
    this.loadSpinner = true;
     this.route.params.subscribe(p => {
       this.routeId = p['id'];
    });
    // this.getStoreImage();
    this.marketDetails.getMarketById(this.routeId).subscribe(details => {
      this.market = details;
      this.marketName = this.market.marketName;
      this.description = this.market.description;
      const confirmedStores = this.market.stores.filter(s => s.storeType === 'physical store');
      this.stores = confirmedStores.filter(cs => cs.confirmStatus === true);
      if (this.stores.length === 0) {
        this.loadSpinner = false;
        this.emptyMarket = true;
      } else {
        this.stores.map(x => {
        });
      }
      this.loadSpinner = false;
    });
  }
  async getStoreImage() {
    this.storeId = this.localStorage.getItem('storeId');
    await this.adminService.getStoreImageById(this.storeId).subscribe(img => {
      this.storeImage = img.file;
    });
  }
  getStoresId(id) {
      this.router.navigate([ 'store',   id]);
  }
}
