import { Component, OnInit, OnDestroy } from '@angular/core';
import { AdminDataService } from 'src/app/admin/admin-services/admin.data.service';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-estore',
  templateUrl: './estore.component.html',
  styleUrls: ['./estore.component.css']
})
export class EstoreComponent implements OnInit, OnDestroy {

  private unsubscribe$ = new Subject<void>();
  allEStores: any;
  emptyMarket: boolean;
  searchText: any;
  loadSpinner: boolean;

  constructor(private adminService: AdminDataService,
              private router: Router) { }

  ngOnInit() {

    this.getAllEstores();

  }

  getAllEstores() {
    this.loadSpinner = true;
    this.adminService.getStores()
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(res => {
      if (!res) {
        this.emptyMarket = true;
        this.loadSpinner = false;
      } else {
        const allStores = res;
      const estores = allStores.filter(s => s.storeType !== 'physical store')
      this.allEStores = estores.filter(cs => cs.confirmStatus === true);
      this.loadSpinner = false;
      }
    });
  }

  getStoresId(id){
    this.router.navigate(['store', id]);
  }
  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
