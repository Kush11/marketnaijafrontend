import { RegisterStoreComponent } from './components/register-store/register-store.component';
import { NgModule } from '@angular/core';
import { HomescreenComponent } from './components/homescreen/homescreen.component';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { AboutComponent } from './components/about/about.component';
import { DisclaimerComponent } from './components/disclaimer/disclaimer.component';
import { DirectionsComponent } from './components/directions/directions.component';
import { HowtoComponent } from './components/howto/howto.component';
import { PolicyComponent } from './components/policy/policy.component';
import { ContactComponent } from './components/contact/contact.component';
import { TermsComponent } from './components/terms/terms.component';
import { AuthGuard } from './_guard/auth.guard';
import { Role } from './Model/role';
import { MarketsComponent } from './components/markets/markets.component';
import { StoreComponent } from './components/store/store.component';
import { IndividualMarketComponent } from './components/individual-market/individual-market.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { EstoreComponent } from './components/estore/estore.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomescreenComponent },
  { path: 'about', component: AboutComponent },
  { path: 'disclaimer', component: DisclaimerComponent },
  { path: 'direction', component: DirectionsComponent },
  { path: 'howto', component: HowtoComponent },
  { path: 'policy', component: PolicyComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'terms', component: TermsComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'markets',
    component: MarketsComponent
  },
  {
    path: 'productDetails/:id',
    component: ProductDetailsComponent
  },
  {
    path: 'markets/:id',
    component: IndividualMarketComponent
  },
  {
    path: 'estore',
    component: EstoreComponent
  },
  {
    path: 'registerStore',
    component: RegisterStoreComponent
  },
  {
    path: 'store/:id',
    component: StoreComponent
  },
  {
    path: 'admin',
    loadChildren: './admin/admin.module#AdminModule'
  },
  // otherwise redirect to home
  { path: '**', redirectTo: 'home', pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
