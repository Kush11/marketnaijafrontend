import { AdminDataService } from 'src/app/admin/admin-services/admin.data.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Resolve } from '@angular/router';

@Injectable()
export class Resolver implements Resolve<Observable<string>> {
  constructor(private adminService: AdminDataService) {}

  resolve() {
    return this.adminService.getProducts();
  }
}
