import { AuthenticateWebService } from './services/Authentication/authentication.web.service';
import { Component, OnInit, Inject } from '@angular/core';
import { Router, NavigationStart, ActivatedRoute, NavigationEnd } from '@angular/router';
import { User } from './Model/user';
import { Role } from './Model/role';
import { Observable } from 'rxjs';
import { WINDOW } from '@ng-toolkit/universal';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  showHead: boolean;
  currentUser: User;
  isLoggedIn$: Observable<boolean>;
  routeId: number;

  constructor(@Inject(WINDOW) private window: Window, private router: Router,
              private authenticate: AuthenticateWebService,
              private route: ActivatedRoute) {

    this.authenticate.currentUser.subscribe(x => this.currentUser = x);
    // on route change to '/login', set the variable showHead to false
    router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        if (event['url'] === '/login' || event['url'].startsWith('/admin') ) {
          this.showHead = false;
        } else {
          // console.log("NU")
          this.showHead = true;
        }
      }
    });
  }


  ngOnInit() {

    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      this.window.scrollTo(0, 0);
  });

    this.route.params.subscribe(p => {
      this.routeId = +p['id'];
    });
   }

  get isAdmin() {
    return this.currentUser &&  this.currentUser.role === Role.Admin;
  }

  logout() {
    this.authenticate.logout();
    this.router.navigate(['/login']);
  }
}
