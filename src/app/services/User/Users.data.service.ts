import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'src/app/Model/user';


@Injectable()
export abstract class AuthenticateUsersDataService {
  abstract getAll(): Observable<User[]>;
  abstract register(user);
  abstract update(user);
  abstract delete(user);
}
