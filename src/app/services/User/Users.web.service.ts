
import { Injectable } from '@angular/core';
import { User } from 'src/app/Model/user';
import { HttpClient } from '@angular/common/http';
import { AuthenticateUsersDataService } from './Users.data.service';




@Injectable()
export abstract class AuthenticateUsersWebService implements  AuthenticateUsersDataService{
  URL: string;

  constructor(public http: HttpClient) {
    this.URL = 'http://localhost:44345/api/user';
}

  getAll() {
    return this.http.get<User[]>(`${this.URL}`);
  }

  getById(id: number) {
    return this.http.get<User>(`${this.URL}/${id}`);
}

register(user: User) {
  return this.http.post(`${this.URL}/register`, user);
}
update(user: User) {
  return this.http.put(`${this.URL}/${user.id}`, user);
}
delete(id: number) {
  return this.http.delete(`${this.URL}/${id}`);
}

}
