import { RegisterStore } from './../../Model/registerStore';
import { Stores} from './../../Model/stores';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { MarketsDataService } from './markets.data.service';
import { Markets } from 'src/app/Model/markets';
import { Review } from 'src/app/Model/review';
import { Customer } from 'src/app/Model/customer';
import { Order } from 'src/app/Model/order';
import { Products } from 'src/app/Model/products';

@Injectable({ providedIn: 'root' })
export class MarketsWebService implements MarketsDataService {


  public webUrl: string;
  public mockUrl: string;
  config: HttpHeaders;


  constructor(private http: HttpClient) {
    this.webUrl = 'https://marketnaijaapp20190720105614.azurewebsites.net/api';
    this.mockUrl = 'https://localhost:44370/api';

  this.config = new HttpHeaders().set('Content-Type', 'application/json')
  .set('Accept', 'application/json');
  }


  getHomeScreenStores() {
    return this.http.get<Markets[]>(`${this.webUrl}/homestores`);
  }
  getHomeScreenProducts() {
    return this.http.get<Markets[]>(`${this.webUrl}/products/homeproduct`);
  }
  getAllMarkets() {
    return this.http.get<Markets[]>(`${this.webUrl}/markets`);
  }

  getMarketById(id: number) {
    return this.http.get<Markets>(`${this.webUrl}/markets/${id}`);
  }

  updateMarket(market: Markets) {
    return this.http.put(`${this.webUrl}/markets/${market.id}`, market);
  }
  delete(id: AnimationPlaybackEvent) {
    return this.http.delete(`${this.webUrl}/markets/${id}`);
  }
  getAllStores() {
    return this.http.get(`${this.webUrl}/stores`);
  }
  getStoresByUserId(id: any) {
    const params = new HttpParams().set('id', `${id}`);
    return this.http.get<Stores>(`${this.webUrl}/stores/users`, {headers: this.config, params: params});
  }
  getStoresById(id: any) {
    return this.http.get<Stores>(`${this.webUrl}/stores/${id}`);
  }
  AddReview(review: Review) {
    return this.http.post<Review>(`${this.webUrl}/review`, JSON.stringify(review), {headers: this.config});
  }
  createStore(store: RegisterStore) {
    return this.http.post<RegisterStore>(`${this.webUrl}/stores`, JSON.stringify(store), {headers: this.config});
  }
  createCustomer(customer: Customer) {
    return this.http.post<Customer>(`${this.webUrl}/customers`, JSON.stringify(customer), {headers: this.config});
  }
  getCustomers() {
    return this.http.get<Customer[]>(`${this.webUrl}/customers`);
  }
  getCustomerById(id: any) {
    const params = new HttpParams().set('id', `${id}`);
   return this.http.get(`${this.webUrl}/customers/store`, {headers: this.config, params: params});
  }
  getProductById(id: any) {
    return this.http.get<Products>(`${this.webUrl}/products/${id}`);
 }
 submitOrder(order: Order) {
   return this.http.post<Order>(`${this.webUrl}/orders`, JSON.stringify(order), {headers: this.config});
 }
 updateOrders(status: any, id: any) {
   return this.http.put(`${this.webUrl}/orders/${id}`, status);
 }
}
