import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Markets } from 'src/app/Model/markets';
import { Stores } from 'src/app/Model/stores';
import { Customer } from 'src/app/Model/customer';
import { Order } from 'src/app/Model/order';
import { Products } from 'src/app/Model/products';



@Injectable()
export abstract class MarketsDataService {
  abstract getAllMarkets(): Observable<Markets[]>;
  abstract getMarketById(id);
  abstract updateMarket(market);
  abstract delete(market);
  abstract getAllStores(): Observable<any>;
  abstract getStoresById(id);
  abstract AddReview(review);
  abstract createStore(store);
  abstract createCustomer(customer);
  abstract getCustomers(): Observable<Customer[]>;
  abstract getStoresByUserId(id);
  abstract getProductById(id): Observable<Products>;
  abstract submitOrder(order): Observable<Order>;
  abstract updateOrders(status, id);
  abstract getHomeScreenStores(): Observable<any>;
  abstract getHomeScreenProducts(): Observable<any>;
  abstract getCustomerById(id): Observable<any>;
}
