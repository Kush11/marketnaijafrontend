import { User } from './../../Model/user';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, config } from 'rxjs';
import { map } from 'rxjs/operators';

import { AuthenticateDataService } from './authentication.data.service';
import * as jwt_decode from 'jwt-decode';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';


@Injectable({ providedIn: 'root' })
export class AuthenticateWebService implements AuthenticateDataService {
  public webUrl: string;
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  public registerUrl: string;
  private loggedIn = new BehaviorSubject<boolean>(false);
  mockUrl: string;

  constructor(@Inject(LOCAL_STORAGE) private localStorage: any, private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(
      JSON.parse(localStorage.getItem('currentUser'))
    );
    this.currentUser = this.currentUserSubject.asObservable();
    this.webUrl =
      'https://marketnaijaapp20190720105614.azurewebsites.net/api';

    this.mockUrl = 'https://localhost:44370/api';
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }
  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }

  login(username: string, password: string) {
    return this.http.post<any>(`${this.webUrl}/user/authenticate`, { username, password }).pipe(
      map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          this.localStorage.setItem('currentUser', JSON.stringify(user));
          this.loggedIn.next(true);
          this.currentUserSubject.next(user);
        }

        return user;
      })
    );
  }

  getAll() {
    return this.http.get<User[]>(`${this.webUrl}/user`);
  }

  getById(id: any) {
    return this.http.get<User>(`${this.webUrl}/user/${id}`);
  }

  register(user: any) {
    console.log('phone', user);
    return this.http.post(`${this.webUrl}/user/register`, user);
  }
  update(user: User) {
    console.log('user id from service',  user.id);
    return this.http.put(`${this.webUrl}/user/${user.id}`, user);

  }
  delete(id: any) {
    return this.http.delete(`${this.webUrl}/user/${id}`);
  }

  logout() {
    // remove user from local storage to log user out
    this.localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }
  decode() {
    const currentUser = JSON.parse(this.localStorage.getItem('currentUser'));
    const token = currentUser.token;
    try {
      return jwt_decode(token);
    } catch (Error) {
     return null;
   }
  }
}
